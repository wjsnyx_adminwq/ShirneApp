import 'dart:ui' show window;

import 'package:flutter/material.dart';

class Config {
  static const int majorVersion = 1;
  static const int minorVersion = 0;
  static const int patchVersion = 0;

  static String version() {
    return majorVersion.toString() +
        '.' +
        minorVersion.toString() +
        '.' +
        patchVersion.toString();
  }

  static const String server = "https://www.shirne.com/api/";
  static const String imgpre = "https://www.shirne.com";

  static double windowWidth = 0;
  static double windowHeight = 0;
  static double pixelRatio = 0;

  static double windowPixelWidth = 0;
  static double windowPixelHeight = 0;

  static init(BuildContext context) {
    windowWidth = window.physicalSize.width;
    windowHeight = window.physicalSize.height;
    pixelRatio = window.devicePixelRatio;

    windowPixelWidth = windowWidth / pixelRatio;
    windowPixelHeight = windowHeight / pixelRatio;
  }
}
