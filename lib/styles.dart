
import 'package:flutter/material.dart';

const H1_TEXT = TextStyle(fontSize: 18, fontWeight: FontWeight.bold);
const H2_TEXT = TextStyle(fontSize: 16, fontWeight: FontWeight.bold);
const H3_TEXT = TextStyle(fontSize: 14, fontWeight: FontWeight.w400);
const H4_TEXT = TextStyle(fontSize: 12, fontWeight: FontWeight.w400);
const H5_TEXT = TextStyle(fontSize: 12, fontWeight: FontWeight.normal);

const M_10 = EdgeInsets.all(10.0);
const MT_10 = EdgeInsets.only(top: 10.0);
const ML_10 = EdgeInsets.only(left: 10.0);
const MR_10 = EdgeInsets.only(right: 10.0);
const MB_10 = EdgeInsets.only(bottom: 10.0);
const MV_10 = EdgeInsets.symmetric(vertical: 10.0);
const MH_10 = EdgeInsets.symmetric(horizontal: 10.0);


