import 'dart:io';

import 'package:event_bus/event_bus.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:jpush_flutter/jpush_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'models/search_arguments.dart';
import 'models/detail_arguments.dart';
import 'models/member_model.dart';
import 'models/siteinfo_model.dart';
import 'models/category_model.dart';
import 'utils/request.dart';
import 'utils/toast.dart';
import 'utils/login.dart' show Login;
import 'pages/swipe.dart';
import 'pages/home/home.dart';
import 'pages/product/index.dart' as product;
import 'pages/product/detail.dart' as product;
import 'pages/product/list.dart' as product;
import 'pages/article/index.dart' as article;
import 'pages/article/detail.dart' as article;
import 'pages/article/list.dart' as article;
import 'pages/channel/index.dart' as channel;
import 'pages/channel/detail.dart' as channel;
import 'pages/channel/list.dart' as channel;
import 'pages/about/index.dart' as about;
import 'pages/member/index.dart' as member;

class DataEvent<T>{
  T? data;

  DataEvent(this.data);

  @override
  String toString() {
    return "DataEvent<$T $data>";
  }
}

class MemberEvent extends DataEvent<MemberModel>{
  MemberEvent(MemberModel? data) : super(data);
}

class SiteinfoEvent extends DataEvent<SiteinfoModel>{
  SiteinfoEvent(SiteinfoModel? data) : super(data);
}

class CartCountEvent extends DataEvent<int>{
  CartCountEvent(int? data) : super(data);
}

class LoginStateEvent extends DataEvent<bool>{
  LoginStateEvent(bool? data) : super(data);
}

class RouteEvent extends DataEvent<String>{
  RouteEvent(String? data) : super(data);
}

class CategoryEvent extends DataEvent<Map<int, List<CategoryModel>>>{
  CategoryEvent(Map<int, List<CategoryModel>>? data) : super(data);
}

class Application extends EventBus {

  Login? login;
  SharedPreferences? spref;
  bool isConnected = false;

  Map<String, dynamic> pageCaches = {};

  Map<int, List<CategoryModel>>? _categories;
  Request? _categoriesRequest;
  MemberModel? _member;
  Request? _memberRequest;
  SiteinfoModel? _siteinfo;
  Request? _siteinfoRequest;
  int _cartCount = 0;
  bool _isLogin = false;

  String _rootRoute = '/';
  Map<String, WidgetBuilder>? routes;
  Map<String, String>? redirects;

  BuildContext? _context;

  JPush? jPush;
  String? regId;

  bool initialized = false;

  static Application? instance;
  static Future<Application> getInstance() async{
    if(instance == null){
      instance = Application();
      await instance!.init();
    }
    return instance!;
  }

  init() async {
    if (initialized) return;
    initialized = true;

    routes = {
      '/home': (context) => HomePage(),
      '/swipe': (context) => SwipePage(),
      '/product': (context) => product.IndexPage(),
      '/product/list': (context) => product.ListPage(
          arguments: SearchArguments.fromObject(ModalRoute.of(context)!
              .settings
              .arguments as Map<String, dynamic>)),
      '/product/detail': (context) => product.DetailPage(
          arguments: DetailArguments.fromObject(ModalRoute.of(context)!
              .settings
              .arguments as Map<String, dynamic>)),
      '/article': (context) => article.IndexPage(),
      '/article/list': (context) => article.ListPage(
          arguments: SearchArguments.fromObject(ModalRoute.of(context)!
              .settings
              .arguments as Map<String, dynamic>)),
      '/article/detail': (context) => article.DetailPage(
          arguments: DetailArguments.fromObject(ModalRoute.of(context)!
              .settings
              .arguments as Map<String, dynamic>)),
      '/channel': (context) => channel.IndexPage(
        channel:(ModalRoute.of(context)!
            .settings
            .arguments! as Map<String, dynamic>)['channel'].toString()
      ),
      '/channel/list': (context) => channel.ListPage(
          arguments:(ModalRoute.of(context)!
              .settings
              .arguments! as Map<String, dynamic>)
      ),
      '/channel/detail': (context) => channel.DetailPage(
          arguments:(ModalRoute.of(context)!
              .settings
              .arguments! as Map<String, dynamic>)
      ),
      '/about': (context) => about.IndexPage(),
      '/member': (context) => member.IndexPage(),
    };
    redirects = {};

    spref = await SharedPreferences.getInstance();

    login = new Login(this);

    _memberRequest = new Request('member/profile');

    _siteinfoRequest = new Request('common/siteinfo');

    _categoriesRequest = new Request('article/get_all_cates');

    await login!.restoreToken();

    startJPush();
  }

  dispose() {
    if (!initialized) return;
    _context = null;

    pageCaches.clear();

    spref = null;

    initialized = false;
  }

  getContext() {
    return _context;
  }

  setContext(context) {
    // print(context);
    _context = context;
  }

  toast(message, [duration = 2]) {
    Toast.show(message, duration: duration);
  }

  Future<SiteinfoModel> getSiteinfo({force = false}) async {
    if (!force && _siteinfo != null && _siteinfo!.name.isNotEmpty) {
      return _siteinfo!;
    }
    var sJson = await _siteinfoRequest!.getData(force: force);
    _siteinfo = SiteinfoModel.fromJson(sJson);
    this.fire(SiteinfoEvent(_siteinfo));
    return _siteinfo!;
  }

  Future<MemberModel> getProfile({force = false}) async {
    if (!force && _member != null && _member!.id > 0) {
      return _member!;
    }
    var mJson = await _memberRequest!.getData(force: force);
    _member = MemberModel.fromJson(mJson);
    this.fire(MemberEvent(_member));
    return _member!;
  }

  Future<Map<int, List<CategoryModel>>> getCategories({force = false}) async {
    if (!force && _categories != null) {
      return _categories!;
    }
    var mJson = await _categoriesRequest!.getData(force: force);
    _categories = CategoryModel.mapTree(mJson);
    this.fire(CategoryEvent(_categories));
    return _categories!;
  }

  set isLogin(bool newValue){
    _isLogin = newValue;
    this.fire(LoginStateEvent(_isLogin));
  }

  bool get isLogin{
    return _isLogin;
  }

  set cartCount(int newValue){
    _cartCount = newValue;
    this.fire(CartCountEvent(_cartCount));
  }

  int get cartCount{
    return _cartCount;
  }

  cachePage(String name, dynamic data) {
    pageCaches[name] = data;
  }

  getPage(String name) {
    return pageCaches.containsKey(name) ? pageCaches[name] : null;
  }

  back(BuildContext context) {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      Navigator.pushReplacementNamed(context, '/');
    }
  }

  route(String url, {Map<String, dynamic>? arguments, String? channel}) {
    if (url.indexOf('/pages/') == 0) {
      url = url.substring(6);
    }
    if (arguments == null) arguments = Map();
    if (url.indexOf('?') > 0) {
      List<String> parts = url.split('?');
      url = parts[0];
      List<String> query = parts[1].split('&');
      query.forEach((item) {
        int idx = item.indexOf('=');
        if (idx > 0) {
          arguments![item.substring(0, idx)] = item.substring(idx + 1);
        }
      });
    }
    if(channel != null){
      arguments['channel'] = channel;
    }
    //不在路由范围内再尝试去除尾部 index
    if (!routes!.containsKey(url) && !redirects!.containsKey(url)) {
      if (url.endsWith('/index')) {
        url = url.substring(0, url.length - 6);
      }
    }
    print(['Navigate ' + url, arguments]);
    if (redirects!.containsKey(url)) {
      print('route redirect:' + url);
      route(redirects![url]!, arguments: arguments);
      return;
    }

    if (!routes!.containsKey(url)) {
      Toast.show('页面不存在');
      return;
    }

    if (url == '/') {
      Navigator.popUntil(
        _context!,
        (route) => route.settings.name == '/',
      );
      print(['tab:', arguments['route']]);

      _rootRoute = arguments['route'].toString();
      this.fire(RouteEvent(_rootRoute));
    } else {
      Navigator.pushNamed(_context!, url, arguments: arguments);
    }
  }

  startJPush() async {
    if(kIsWeb || !(Platform.isAndroid || Platform.isIOS)){
      print(r'Not android or ios, skip jpush');
      return;
    }
    jPush = new JPush();
    jPush!.addEventHandler(
      // 接收通知回调方法。
      onReceiveNotification: (Map<String, dynamic> message) async {
        print("flutter onReceiveNotification: $message");
      },
      // 点击通知回调方法。
      onOpenNotification: (Map<String, dynamic> message) async {
        print("flutter onOpenNotification: $message");
      },
      // 接收自定义消息回调方法。
      onReceiveMessage: (Map<String, dynamic> message) async {
        print("flutter onReceiveMessage: $message");
      },
    );
    jPush!.setup(
      appKey: "62eb07d227d1f11dd7fa6239",
      channel: "developer-default",
      production: false,
      debug: true, // 设置是否打印 debug 日志
    );
    jPush!.getRegistrationID().then((rid) {
      regId = rid;
      print('get registrationid: ' + rid);
    });
  }
}
