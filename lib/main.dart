
import 'dart:io' show Platform;

import 'package:catcher/core/catcher.dart';
import 'package:catcher/handlers/console_handler.dart';
import 'package:catcher/handlers/email_manual_handler.dart';
import 'package:catcher/mode/dialog_report_mode.dart';
import 'package:catcher/model/catcher_options.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:window_size/window_size.dart';
import 'application.dart';
import 'config.dart';
import 'pages/index.dart';

void main() {
  CatcherOptions debugOptions =
      CatcherOptions(DialogReportMode(), [ConsoleHandler()]);
  CatcherOptions releaseOptions = CatcherOptions(DialogReportMode(), [
    EmailManualHandler(["shirne@126.com"])
  ]);

  Catcher(
      rootWidget: ShirneApp(),
      debugConfig: debugOptions,
      releaseConfig: releaseOptions,
  );
}

class ShirneApp extends StatefulWidget {
  @override
  State<ShirneApp> createState() => _ShirneAppState();
}

class _ShirneAppState extends State<ShirneApp> {
  MaterialColor mColor = Colors.blue;
  bool initialized = false;
  late Application app;

  @override
  void initState() {
    super.initState();

    _init();
  }

  _init() async {
    Config.init(context);
    app = await Application.getInstance();

    setState(() {
      initialized = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return initialized
        ? MaterialApp(
            title: '',
            onGenerateTitle: (BuildContext context) {
              if (!kIsWeb &&
                  (Platform.isWindows ||
                      Platform.isMacOS ||
                      Platform.isLinux)) {
                getWindowInfo().then((window) {
                  setWindowTitle('临风小筑');
                });
              }
              return '临风小筑';
            },
            navigatorKey: Catcher.navigatorKey,
            theme: ThemeData(
              primarySwatch: mColor,
            ),
            home: IndexPage(),
            routes: app.routes!,
          )
        : Container(
            color: Colors.blueAccent,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
  }
}
