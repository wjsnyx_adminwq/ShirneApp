

import 'package:flutter/material.dart';

class SkeletonWidget extends StatelessWidget{
  final int row;
  final bool title;
  final bool avatar;
  final bool round;
  final double avatarSize;
  final String avatarShape;
  final double titleWidth;
  final Gradient gradient;

  const SkeletonWidget({
    Key? key,
    this.row = 2,
    this.title = true,
    this.avatar = false,
    this.round = false,
    this.avatarSize = 32,
    this.avatarShape = r'round',
    this.titleWidth = .5,
    this.gradient = const LinearGradient(colors: [Color.fromRGBO(233, 233, 233, 1), Color.fromRGBO(222, 222, 222, 1)]),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Row(
      children: [
        if(avatar) Container(
          decoration: BoxDecoration(gradient: gradient),
          width: avatarSize,
          height: avatarSize,
          margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
        ),
        Column(
          children: [
            if(title) Container(
              decoration: BoxDecoration(gradient: gradient),
              width: width * titleWidth,
              margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
            ),
            ...List.generate(row, (index) => Container(
              decoration: BoxDecoration(gradient: gradient),
              margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
            ))
          ],
        )
      ],
    );
  }

}