import 'package:flutter/material.dart';

class TagDivider extends StatelessWidget {
  final String text;
  final BorderStyle lineStyle;
  final AlignmentGeometry textAlign;
  final double padding;
  final double vPadding;
  final Color background;
  final Color color;

  const TagDivider(this.text,
      {Key? key,
      this.lineStyle = BorderStyle.solid,
      this.textAlign = Alignment.center,
      this.padding = 20,
      this.vPadding = 10,
      this.background = Colors.transparent,
      this.color = Colors.black38})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Padding textWidget = Padding(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Text(
        text,
        style: TextStyle(color: color, fontSize: 12),
      ),
    );
    List<Widget> childs = [];
    if (textAlign == Alignment.centerLeft) {
      childs.add(textWidget);
    }
    childs.add(Expanded(child: Divider()));
    if (textAlign == Alignment.center) {
      childs.add(textWidget);
      childs.add(Expanded(child: Divider()));
    }
    if (textAlign == Alignment.centerRight) {
      childs.add(textWidget);
    }

    return Container(
      padding: EdgeInsets.symmetric(vertical: padding, horizontal: vPadding),
      color: background,
      child: Container(
        height: 0,
        child: OverflowBox(
          maxHeight: padding * 2,
          child: Row(
            children: childs,
          ),
        ),
      ),
    );
  }
}
