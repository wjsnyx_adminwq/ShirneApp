import 'package:flutter/material.dart';

class TagCellGroup extends StatelessWidget {
  final EdgeInsetsGeometry margin;
  final Color color;

  final List<Widget> children;

  const TagCellGroup(
      {Key? key,
      this.margin = const EdgeInsets.only(top: 10),
      this.color = Colors.white,
      required this.children})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color,
      margin: margin,
      child: Column(
        children: children,
      ),
    );
  }
}
