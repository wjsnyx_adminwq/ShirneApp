import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TagButton extends StatelessWidget {
  final String label;
  final IconData? icon;
  final Color color;
  final Color? borderColor;
  final Color textColor;
  final double fontSize;
  final double fontHeight;
  final double horizontalPadding;
  final Function? onPressed;

  final BoxShape shape;
  final double? borderRadius;

  const TagButton(this.label,
      {Key? key,
      this.icon,
      this.color = Colors.red,
      this.textColor = Colors.white,
      this.fontSize = 12.0,
      this.fontHeight = 2.0,
      this.horizontalPadding = 10,
      this.borderColor,
      this.onPressed,
      this.shape = BoxShape.rectangle,
      this.borderRadius})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    BorderRadius? radius;
    if (shape == BoxShape.rectangle) {
      radius = BorderRadius.circular(
          borderRadius == null ? (fontSize * 2) : borderRadius!);
    }
    return GestureDetector(
      onTap: () {
        if (onPressed != null) onPressed!();
      },
      child: Container(
        height: fontSize * fontHeight,
        decoration: BoxDecoration(
          color: color,
          shape: shape,
          border: borderColor == null
              ? null
              : Border.all(color: borderColor!, width: 0.5),
          borderRadius: radius,
        ),
        padding: EdgeInsets.symmetric(
            vertical: (fontHeight - 1) * fontSize / 2,
            horizontal: horizontalPadding),
        child: icon == null
            ? Text(
                label,
                style:
                    TextStyle(fontSize: fontSize, color: textColor, height: 1),
                strutStyle: StrutStyle(height: 1),
              )
            : Row(
                children: <Widget>[
                  Icon(
                    icon,
                    color: textColor,
                    size: fontSize,
                  ),
                  Text(
                    label,
                    style: TextStyle(
                        fontSize: fontSize, color: textColor, height: 1),
                    strutStyle: StrutStyle(height: 1),
                  )
                ],
              ),
      ),
    );
  }
}
