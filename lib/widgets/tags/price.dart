import 'package:flutter/material.dart';

class TagPrice extends StatelessWidget {
  final double price;
  final Color color;
  final double fontSize;
  final bool lineThrough;

  const TagPrice(this.price,
      {Key? key,
      this.color = Colors.red,
      this.fontSize = 14,
      this.lineThrough = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Text(
          '￥',
          style: TextStyle(color: color, fontSize: fontSize * 0.6),
        ),
        Text(
          price.toStringAsFixed(2),
          style: TextStyle(fontSize: fontSize, color: color),
        )
      ],
    );
  }
}
