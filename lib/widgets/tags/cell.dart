import 'package:flutter/material.dart';

class TagCell extends StatelessWidget {
  final String title;
  final double titleSize;
  final IconData? icon;
  final IconData? rightIcon;
  final Widget? child;
  final String? desc;
  final Function? onTap;
  final double verticalPadding;
  final double horizontalPadding;
  final Color titleColor;
  final Color iconColor;
  final Color descColor;
  final Color rightIconColor;
  final EdgeInsetsGeometry? margin;
  final Color backgroundColor;

  const TagCell({
    Key? key,
    required this.title,
    this.icon,
    this.desc,
    this.onTap,
    this.verticalPadding = 10,
    this.horizontalPadding = 10,
    this.titleColor = Colors.black54,
    this.descColor = Colors.black12,
    this.margin,
    this.child,
    this.titleSize = 14,
    this.iconColor = Colors.black38,
    this.rightIcon = Icons.chevron_right,
    this.rightIconColor = Colors.grey,
    this.backgroundColor = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> childs = [];

    if (icon != null) {
      childs.add(Icon(
        icon,
        size: titleSize,
        color: iconColor,
      ));
    }
    childs.add(Text(
      title,
      style: TextStyle(fontSize: titleSize, color: titleColor),
    ));

    childs.add(Expanded(child: child == null ? Text('') : child!));
    if (desc != null) {
      childs.add(Text(
        desc!,
        style: TextStyle(color: descColor, fontSize: 13),
      ));
    }

    if (rightIcon != null) {
      childs.add(Icon(
        rightIcon,
        size: titleSize,
        color: rightIconColor,
      ));
    }

    return GestureDetector(
      onTap: () {
        if (onTap != null) {
          onTap!();
        }
      },
      child: Container(
        color: backgroundColor,
        padding: EdgeInsets.symmetric(horizontal: 10),
        margin: margin,
        child: Container(
          padding: EdgeInsets.symmetric(vertical: verticalPadding),
          decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    color: Color.fromRGBO(232, 232, 232, 1), width: 0.5)),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: childs,
          ),
        ),
      ),
    );
  }
}
