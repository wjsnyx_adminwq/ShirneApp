import 'package:flutter/material.dart';

class TagEmpty extends StatelessWidget {
  final String text;
  final IconData? icon;
  final double paddingTop;
  final double paddingBottom;
  final Color color;

  const TagEmpty(this.text,
      {Key? key,
      this.icon = Icons.format_list_bulleted,
      this.paddingTop = 30.0,
      this.paddingBottom = 10.0,
      this.color = Colors.grey})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: paddingTop, bottom: paddingBottom),
      child: Column(
        children: <Widget>[
          icon == null
              ? SizedBox()
              : Center(
                  child: Icon(
                  icon,
                  size: 50.0,
                  color: color,
                )),
          icon == null
              ? SizedBox()
              : SizedBox(
                  height: 10,
                ),
          Center(
            child: Text(
              text,
              style: TextStyle(color: color, fontSize: 14),
            ),
          )
        ],
      ),
    );
  }
}
