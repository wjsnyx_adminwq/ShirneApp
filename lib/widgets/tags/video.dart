import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import '../../utils/tools.dart';

import 'icon.dart';

class TagVideo extends StatefulWidget {
  final String video;
  final String? thumb;
  final double width;
  final double? height;
  final bool quick;
  final Function? onThumb;

  const TagVideo(this.video,
      {Key? key,
      this.thumb,
      required this.width,
      this.height,
      this.quick = false,
      this.onThumb})
      : super(key: key);

  @override
  State<TagVideo> createState() => _TagVideoState();
}

class _TagVideoState extends State<TagVideo>
    with AutomaticKeepAliveClientMixin<TagVideo> {
  late VideoPlayerController _controller;
  bool isPlaying = false;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(widget.video);
    _controller.addListener(_onVideo);
    Future.delayed(Duration(milliseconds: widget.quick ? 0 : 300), () {
      _controller.initialize().then((_) {
        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        setState(() {});
      });
    });
  }

  @override
  void dispose() {
    _controller.removeListener(_onVideo);
    _controller.dispose();
    super.dispose();
  }

  _onVideo() {
    if (_controller.value.isPlaying != isPlaying) {
      setState(() {
        isPlaying = _controller.value.isPlaying;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    var bannerWidth = widget.width;
    var bannerHeight = widget.height;
    if (bannerHeight == null || bannerHeight <= 0) {
      if (_controller.value.isInitialized) {
        bannerHeight = bannerWidth * _controller.value.aspectRatio;
      } else {
        bannerHeight = bannerWidth * 0.6;
      }
    }
    return Material(
      child: Stack(
        fit: StackFit.expand,
        alignment: Alignment.topCenter,
        children: [
          Container(
            child: _controller.value.isInitialized
                ? Container(
                    color: Colors.black,
                    child: Center(
                      child: AspectRatio(
                        aspectRatio: _controller.value.aspectRatio,
                        child: VideoPlayer(_controller),
                      ),
                    ),
                  )
                : Container(
                    child: Tools.image(widget.thumb!,
                        fit: BoxFit.cover,
                        width: bannerWidth,
                        height: bannerHeight)),
          ),
          Center(
            child: isPlaying
                ? null
                : IconButton(
                    icon: TagIcon(
                      Icons.play_circle_outline,
                      size: 48,
                      color: Colors.white,
                      shadows: [Shadow(color: Colors.black, blurRadius: 10)],
                    ),
                    onPressed: () {
                      _controller.play();
                    }),
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: isPlaying
                ? IconButton(
                    icon: TagIcon(
                      Icons.pause,
                      color: Colors.white,
                      shadows: [Shadow(color: Colors.black, blurRadius: 10)],
                    ),
                    onPressed: () {
                      _controller.pause();
                    })
                : null,
          )
        ],
      ),
    );
  }
}
