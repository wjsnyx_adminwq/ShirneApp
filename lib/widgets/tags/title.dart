import 'package:flutter/material.dart';

class TagTitle extends StatelessWidget {
  final String title;
  final int maxLine;
  final TextAlign textAlign;
  final TextOverflow overflow;
  final Color color;
  final double fontSize;
  final FontWeight fontWeight;
  final EdgeInsets? margin;

  const TagTitle(this.title,
      {Key? key,
      this.maxLine = 1,
      this.textAlign = TextAlign.start,
      this.overflow = TextOverflow.ellipsis,
      this.color = Colors.black87,
      this.fontSize = 14,
      this.fontWeight = FontWeight.w400,
      this.margin})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin == null ? EdgeInsets.only(top: 5, bottom: 10) : margin,
      child: Text(
        title,
        style:
            TextStyle(color: color, fontSize: fontSize, fontWeight: fontWeight),
        softWrap: true,
        strutStyle: StrutStyle(height: 1.2),
        textAlign: textAlign,
        maxLines: maxLine,
        overflow: overflow,
      ),
    );
  }
}
