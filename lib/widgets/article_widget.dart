import 'package:flutter/material.dart';

import '../utils/tools.dart';
import '../models/article_model.dart';
import '../config.dart';

class ArticleWidget extends StatelessWidget {
  final ArticleModel item;

  final int cols;

  const ArticleWidget(this.item, {Key? key, this.cols = 3}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var width = (Config.windowPixelWidth - 10 * cols) / cols;

    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, '/article/detail',
            arguments: {"id": item.id, "data": item});
      },
      child: Container(
        color: Colors.white,
        width: width,
        child: Column(
          children: [
            Center(
                child: Hero(
                    tag: "article_" + item.id.toString(),
                    child:
                        Tools.image(item.cover, width: width, height: width))),
            Container(
              padding: EdgeInsets.only(bottom: 10),
              child: Text(
                item.title,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    decoration: TextDecoration.none),
                maxLines: 2,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
