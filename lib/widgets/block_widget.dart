import 'package:flutter/cupertino.dart';

class BlockWidget extends StatelessWidget {
  final String title;
  final Widget child;
  final Widget? footer;

  const BlockWidget(
      {Key? key, required this.title, required this.child, this.footer})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var childs = <Widget>[
      Row(
        children: <Widget>[Text(title)],
      ),
      Row(
        children: <Widget>[child],
      )
    ];
    if (footer != null) {
      childs.add(Row(
        children: <Widget>[footer!],
      ));
    }

    return Container(
      child: Column(children: childs),
    );
  }
}
