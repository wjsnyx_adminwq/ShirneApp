import 'package:flutter/material.dart';

import '../utils/tools.dart';
import '../models/product_model.dart';
import '../config.dart';

class ProductWidget extends StatelessWidget {
  final ProductModel item;

  final int cols;

  const ProductWidget(this.item, {Key? key, this.cols = 3}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var width = (Config.windowPixelWidth - 10 * 2 - 10 * (cols - 1)) / cols;

    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, '/product/detail',
            arguments: {"id": item.id});
      },
      child: Container(
        color: Colors.white,
        width: width,
        child: Column(
          children: [
            Center(
              child: Hero(
                tag: "product_" + item.id.toString(),
                child: item.image.isEmpty
                    ? Image.asset('assets/graphics/blank.png')
                    : FadeInImage.assetNetwork(
                        placeholder: 'assets/graphics/blank.png',
                        fadeInDuration: Duration(seconds: 2),
                        image: Tools.fixImageUrl(item.image),
                        fit: BoxFit.cover,
                      ),
              ),
            ),
            Center(
              child: Text(
                item.title,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    decoration: TextDecoration.none),
                maxLines: 2,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
