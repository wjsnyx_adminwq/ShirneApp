import 'package:flutter/material.dart';
import 'tags/title.dart';

class CardWidget extends StatelessWidget {
  final Widget? title;
  final Widget? footer;
  final Widget? child;

  final Color color;
  final double borderRadius;
  final String? titleText;

  const CardWidget(
      {Key? key,
      this.title,
      this.footer,
      this.child,
      this.titleText,
      this.color = Colors.white,
      this.borderRadius = 5})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [];

    if (title == null) {
      if (titleText != null && titleText!.length > 0) {
        children.add(Row(
          children: <Widget>[TagTitle(titleText!)],
        ));
      }
    } else {
      children.add(title!);
    }
    if (children.length > 0) {
      children.add(Divider(
        thickness: 0.5,
        height: 1,
      ));
    }

    if (child != null) {
      children.add(child!);
    }

    if (footer != null) {
      children.add(Divider(
        thickness: 0.5,
        height: 1,
      ));
      children.add(footer!);
    }

    return Card(
      color: color,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(borderRadius))),
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          children: children,
        ),
      ),
    );
  }
}
