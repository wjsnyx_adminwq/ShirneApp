import 'package:flutter/material.dart';

abstract class StatefulPage extends StatefulWidget {
  final Map<String, dynamic> arguments;

  const StatefulPage({Key? key, required this.arguments}) : super(key: key);
}

abstract class StatelessPage extends StatelessWidget {
  final Map<String, dynamic> arguments;

  const StatelessPage({Key? key, required this.arguments}) : super(key: key);
}
