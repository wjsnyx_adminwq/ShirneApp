import 'api.dart';

class Request {
  String _api = '';
  Map<String, dynamic> _param;
  var tryTimes = 0;
  var maxTimes = 10;

  var errorTimes = 0;

  Map<String, dynamic>? data;
  bool isLoading = false;

  Request(this._api, [this._param = const {}]);

  setParam(String key, Object val) {
    _param[key] = val;
  }

  getParam(String key) {
    return _param[key];
  }

  getData({force = false}) async {
    if (data == null || force) {
      data = await _request();
    }
    return data;
  }

  _request() async {
    print('request');
    var result = await Api.post(_api, _param);
    return result.data;
  }
}
