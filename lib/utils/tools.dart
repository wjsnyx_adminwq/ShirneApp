import 'package:flutter/material.dart';
import 'package:intl/intl.dart';


import '../config.dart';

class Tools {

  static fixImageUrl(String? url) {
    if (url == null || url.isEmpty) return '';
    if (url.startsWith('http://') || url.startsWith('https://')) return url;
    return Config.imgpre + url;
  }

  static fixContentImageUrl(String? html) {
    if (html == null || html.isEmpty) return html;
    //html = html.replaceAll(RegExp(r'&emsp;'),'');
    //html = html.replaceAll(RegExp(r'\bid="[^"]+"\s*'), '');

    /*html = html.replaceAllMapped(RegExp(r'<([\w]+)\s+(?:class="([^"]+)")?'),(Match match){
        //print(match)
        if(match[1]=='br'){
            return match[0];
        }else{
            return '<'+match[1]+' class="tag_'+match[1]+(match[2].isEmpty?(' '+match[2]):'')+'" ';
        }
    });*/
    html = html.replaceAllMapped(RegExp(r'src="([^"]+)"'), (Match match) {
      //print(match);
      return 'src="' + fixImageUrl(match[1]!) + '"';
    });
    return html;
  }

  static String formatDate(DateTime date,
      {String format = 'yyyy-MM-dd HH:mm'}) {
    var formatter = new DateFormat(format);
    return formatter.format(date);
  }

  static String formatDateFromTimestamp(int? timeStamp,
      {String format = 'yyyy-MM-dd HH:mm'}) {
    if (timeStamp == null || timeStamp <= 0) return '';
    DateTime date = DateTime.fromMillisecondsSinceEpoch(timeStamp * 1000);
    return formatDate(date, format: format);
  }

  static String parseString(dynamic val) {
    if (val == null) return '';
    if (val is String) return val;
    return val.toString();
  }

  static int parseInt(dynamic number, {int lerant = 0}) {
    return parseNumber(number, lerant: lerant.toDouble()).toInt();
  }

  static double parseNumber(dynamic number, {double lerant = 0.0}) {
    if (number == null) return lerant;
    String type = number.runtimeType.toString();
    if (type == 'String') {
      var result = double.tryParse(number);
      if (result != null) {
        return result;
      }
    } else if (type == 'int') {
      return number.toDouble();
    } else if (type == 'double') {
      return number;
    }

    return lerant;
  }

  static double parseDiySize(dynamic number, {double lerant = 0}) {
    return parseNumber(number, lerant: lerant) / 2;
  }

  static Color transColor(String? hash) {
    return Color(parseColor(hash));
  }

  static int parseColor(String? hash) {
    if (hash == null || hash.length < 3) return 4294967295;
    if (hash.indexOf('#') == 0) hash = hash.substring(1);

    if (hash.length == 3) {
      hash = hash[0] + hash[0] + hash[1] + hash[1] + hash[2] + hash[3];
    }

    if (hash.length == 6) {
      hash = 'FF' + hash;
    }

    if (hash.length == 8) {
      return int.tryParse(hash, radix: 16) ?? 4294967295;
    }
    return 4294967295;
  }

  static Color greyColor = transColor('C8C8CD');
  static Color greyLightColor = transColor('f8f8f8');

  static BorderSide greyBorder =
      BorderSide(color: transColor('C8C8CD'), width: 0.5);

  static Widget avatar(String? url, {size = 55.0}) {
    return image(url,
        placeholder: 'assets/graphics/avatar-default.png',
        width: size,
        height: size);
  }

  static Widget image(String? url,
      {placeholder = 'assets/graphics/blank.png',
      double? width,
      double? height,
      fit: BoxFit.cover}) {
    if (url == null || url.isEmpty) {
      return Image.asset(
        placeholder,
        width: width,
        height: height,
        fit: fit,
      );
    }
    return FadeInImage.assetNetwork(
      placeholder: placeholder,
      image: url,
      width: width,
      height: height,
      fit: fit,
      fadeInDuration: Duration(milliseconds: 500),
    );
  }
}
