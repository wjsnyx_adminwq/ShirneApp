import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';

import '../config.dart';

class Api {
  static _Api api = _Api();

  static newInstance(){
    return _Api();
  }

  static void addHeader(String key, String value) {
    api.addHeader(key, value);
  }

  static void removeHeader(String key) {
    api.removeHeader(key);
  }

  static Future<Result> get(String path) async {
    return await api.get(path);
  }

  static Future<Result> post(String path, [Map<String, dynamic>? data]) async {
    return await api.post(path, data);
  }
}

class _Api {
  late HttpClient client;
  late String apiServer;
  late String userAgent;
  Map<String, String> headers = Map();

  _Api() {
    client = new HttpClient();
    apiServer = Config.server;
    var version = '';
    try {
      version = Platform.version;
    } catch (ex) {}
    userAgent =
    "Mozilla/5.0 (${Platform.operatingSystem}/${Platform.operatingSystemVersion} "
        "Flutter/$version ) "
        "ShirneCMS/${Config.version()} ";
    //print(userAgent);
    if (!apiServer.endsWith('/')) {
      apiServer += '/';
    }

    headers['user-agent'] = userAgent;
  }

  void addHeader(String key, String value) {
    headers[key] = value;
  }

  void removeHeader(String key) {
    if (headers.containsKey(key)) {
      headers.remove(key);
    }
  }

  Future<Result> get(String path) async {
    var response =
    await Dio().get<Map<String, dynamic>>(apiServer + path, options: Options(headers: headers));
    return Result.response(response);
  }

  Future<Result> post(String path, [Map<String, dynamic>? data]) async {
    var response = await Dio()
        .post(apiServer + path, data: data, options: Options(headers: headers));
    return Result.response(response);
  }
}

class Result {
  int statusCode = 200;
  int code = 0;
  String message = '';
  String? _body;
  Map<String, dynamic>? data;
  List<dynamic>? list;

  Result.response(Response response) {
    this.statusCode = response.statusCode!;
    this.init(response.data);
  }

  Result(int statusCode, String body) {
    this.statusCode = statusCode;
    this._body = body;
    if (_body!.isNotEmpty) {
      var resultJson = json.decode(_body!);

      this.init(resultJson);
    }
  }

  init(resultJson) {
    if (resultJson != null && resultJson is Map<String, dynamic>) {
      code = resultJson['code'];
      message = resultJson['message'] ?? '';

      if (code == 1 &&
          resultJson['data'] != null &&
          resultJson['data'].isNotEmpty) {
        if (resultJson['data'] is Map) {
          data = resultJson['data'];
        } else if (resultJson['data'] is List) {
          list = resultJson['data'];
        }
      }
    }
  }
}
