import 'package:flutter/material.dart';

class FittedBoxPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _FittedBoxPageState();
}

class _FittedBoxPageState extends State<FittedBoxPage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('FittedBox'),
      ),
      body: GridView.count(
        crossAxisCount: 4,
        children: <Widget>[
          FittedBox(
            child: Column(
              children: <Widget>[
                Image.asset('assets/graphics/avatar-default.png'),
                Text(
                  'Test name',
                  style: TextStyle(fontSize: 16, color: Colors.blueAccent),
                )
              ],
            ),
          ),
          FittedBox(
            child: Column(
              children: <Widget>[
                Image.asset('assets/graphics/avatar-default.png'),
                Text(
                  'Test name',
                  style: TextStyle(fontSize: 16, color: Colors.blueAccent),
                )
              ],
            ),
          ),
          FittedBox(
            child: Column(
              children: <Widget>[
                Image.asset('assets/graphics/avatar-default.png'),
                Text(
                  'Test name',
                  style: TextStyle(fontSize: 16, color: Colors.blueAccent),
                )
              ],
            ),
          ),
          FittedBox(
            child: Column(
              children: <Widget>[
                Image.asset('assets/graphics/avatar-default.png',width: 20,height: 20,),
                Text(
                  'Test name',
                  style: TextStyle(fontSize: 16, color: Colors.blueAccent),
                )
              ],
            ),
          ),
          FittedBox(
            child: Column(
              children: <Widget>[
                Image.asset('assets/graphics/avatar-default.png'),
                Text(
                  'Test name',
                  style: TextStyle(fontSize: 16, color: Colors.blueAccent),
                )
              ],
            ),
          ),
          FittedBox(
            alignment: Alignment.bottomCenter,
            child: Column(
              children: <Widget>[
                Image.asset('assets/graphics/avatar-default.png',width: 50,height: 50,),
                Text(
                  'Test name',
                  style: TextStyle(fontSize: 16, color: Colors.blueAccent),
                )
              ],
            ),
          ),
           Column(
              children: <Widget>[
                Image.asset('assets/graphics/avatar-default.png'),
                Text(
                  'Test name',
                  style: TextStyle(fontSize: 16, color: Colors.blueAccent),
                )
              ],
            ),
        ],
      ),
    );
  }
}
