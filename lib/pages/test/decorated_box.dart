import 'package:flutter/material.dart';

class DecoratedBoxPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('DecoratedBox'),
      ),
      body: Container(
        child: Center(
          child: DecoratedBox(
            decoration: BoxDecoration(
              gradient: RadialGradient(
                center: const Alignment(-0.5, -0.6),
                radius: 0.15,
                colors: <Color>[
                  const Color(0xFFEEEEEE),
                  const Color(0xFF111133),
                ],
                stops: <double>[0.9, 1.0],
              ),
            ),
            child: Container(
              width: 200,
              height: 200,
            ),
          ),
        ),
      ),
    );
  }
}
