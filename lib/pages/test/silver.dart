import 'package:flutter/material.dart';

class SilverPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SilverPageState();
  }
}

class _SilverPageState extends State<SilverPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          const SliverAppBar(
            pinned: true,
            expandedHeight: 250.0,
            iconTheme: IconThemeData(color: Colors.black87),
            backgroundColor: Colors.white,
            //title: Text('Demo2',style: TextStyle(color: Colors.black87),),
            flexibleSpace: FlexibleSpaceBar(
              collapseMode: CollapseMode.pin,
              //titlePadding: EdgeInsets.all(0),
              centerTitle: true,
              title: Text('Demo',style: TextStyle(color: Colors.blueGrey),),
            ),
          ),
          SliverGrid(
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 200.0,
              mainAxisSpacing: 10.0,
              crossAxisSpacing: 10.0,
              childAspectRatio: 4.0,
            ),
            delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                return Container(
                  alignment: Alignment.center,
                  color: Colors.teal[100 * (index % 9)],
                  child: Text('Grid Item $index'),
                );
              },
              childCount: 20,
            ),
          ),
          SliverFixedExtentList(
            itemExtent: 50.0,
            delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                return Container(
                  alignment: Alignment.center,
                  color: Colors.lightBlue[100 * (index % 9)],
                  child: Text('List Item $index'),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
