import 'package:flutter/material.dart';
import 'package:shirne_app/pages/test/chips.dart';

import 'backdrop.dart';
import 'dragable.dart';
import 'progress.dart';
import 'silver.dart';
import 'constrained_box.dart';
import 'decorated_box.dart';
import 'fitted.dart';
import 'geolocator.dart';
import 'simple_dialog.dart';
import 'search.dart';

class IndexPage extends StatefulWidget {
  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  String _title = "Test";

  @override
  Widget build(BuildContext context) {
    //arguments = ModalRoute.of(context).settings.arguments;
    const sizeBox = SizedBox(height: 10,);
    return Scaffold(
      appBar: AppBar(
        title: Text(_title),
      ),
      body: ListView(
          children: [
            SizedBox(height: 20,),
        Center(
            child: ElevatedButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ProgressPage()));
                },
                child: Text('Progress page'))),
            sizeBox,
        Center(
            child: ElevatedButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => DragablePage()));
                },
                child: Text('Gesture Drag page'))),
            sizeBox,
        Center(
            child: ElevatedButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SilverPage()));
                },
                child: Text('Silver page'))),
            sizeBox,
        Center(
          child: ElevatedButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => BackdropPage()));
            },
            child: Text('BackdopFilter page'),
          ),
        ),
            sizeBox,
        Center(
          child: ElevatedButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => DecoratedBoxPage()));
            },
            child: Text('DecoratedBox page'),
          ),
        ),
            sizeBox,
        Center(
          child: ElevatedButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ConstrainedBoxPage()));
            },
            child: Text('Constrained/Overfflow Box page'),
          ),
        ),
            sizeBox,
        Center(
          child: ElevatedButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => FittedBoxPage()));
            },
            child: Text('FittedBox page'),
          ),
        ),
            sizeBox,
        Center(
          child: ElevatedButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SimpleDialogPage()));
            },
            child: Text('SimpleDialog page'),
          ),
        ),
            sizeBox,
        Center(
          child: ElevatedButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => GeolocatorPage()));
            },
            child: Text('Geolocator page'),
          ),
        ),
            sizeBox,
        Center(
          child: ElevatedButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SearchPage()));
            },
            child: Text('Search page'),
          ),
        ),
            sizeBox,
        Center(
          child: ElevatedButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ChipsPage()));
            },
            child: Text('Chips page'),
          ),
        ),
            sizeBox,
      ]),
    );
  }
}
