import 'package:flutter/material.dart';

class ChipsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ChipsPageState();
}

class _ChipsPageState extends State<ChipsPage> {
  int? _value;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text('chips'),
        ),
        body: ListView(
          children: <Widget>[
            Center(
                child: Wrap(
              children: List<Widget>.generate(
                3,
                (int index) {
                  return ChoiceChip(
                    label: Text('Item $index'),
                    selected: _value == index,
                    onSelected: (bool selected) {
                      setState(() {
                        _value = selected ? index : null;
                      });
                    },
                  );
                },
              ).toList(),
            )),
            Center(
              child: InputChip(
                  avatar: CircleAvatar(
                    backgroundColor: Colors.grey.shade800,
                    child: Text('AB'),
                  ),
                  label: Text('Aaron Burr'),
                  onPressed: () {
                    print('I am the one thing in life.');
                  }),
            )
          ],
        ));
  }
}
