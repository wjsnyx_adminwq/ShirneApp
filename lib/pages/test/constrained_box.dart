import 'package:flutter/material.dart';

class ConstrainedBoxPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ConstrainedBoxPageState();
}

class _ConstrainedBoxPageState extends State<ConstrainedBoxPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Constrained and Overflow box'),
      ),
      body: Column(
        children: <Widget>[
          Center(
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 200,
              color: Colors.lightBlue,
              child: ConstrainedBox(
                constraints: const BoxConstraints.expand(),
                child: const Card(child: Text('Hello World!')),
              ),
            ),
          ),
          Center(
            child: Container(
              width: 100,
              height: 100,
              color: Colors.green,
              child: OverflowBox(
                maxHeight: 300,
                minHeight: 200,
                maxWidth: 150,
                alignment: Alignment.bottomRight,
                child: Container(
                  width: 200,
                  height: 200,
                  color: Color.fromRGBO(0, 150, 50, 0.3),
                  child: Text('Overflow'),
                ),
              ),
            ),
          ),
          Container(
            color: Colors.blueAccent,
            height: 100,
            child: Center(
              child: Text(
                'other content',
                style: TextStyle(color: Colors.white),
              ),
            ),
          )
        ],
      ),
    );
  }
}
