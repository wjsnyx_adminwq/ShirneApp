import 'package:flutter/material.dart';
import '../../models/category_model.dart';
import '../../widgets/tags/cell.dart';

class SimpleDialogPage extends StatefulWidget {
  @override
  State<SimpleDialogPage> createState() => _SimpleDialogPageState();
}

class _SimpleDialogPageState extends State<SimpleDialogPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Simple Dialog page'),
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            Center(
              child: RaisedButton(
                onPressed: () {
                  _askedToLead();
                },
                child: Text('select button'),
              ),
            ),
            Center(
              child: RaisedButton(
                onPressed: () {
                  _ListDialog();
                },
                child: Text('diy content'),
              ),
            ),
            Center(
              child: RaisedButton(
                onPressed: () {
                  _neverSatisfied();
                },
                child: Text('Alert dialog'),
              ),
            ),
            Center(
              child: RaisedButton(
                onPressed: () {
                  _WidgetDialog();
                },
                child: Text('diy widget'),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _askedToLead() async {
    switch (await showDialog<Department>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: const Text('Select assignment'),
            children: <Widget>[
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, Department.treasury);
                },
                child: const Text('Treasury department'),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, Department.state);
                },
                child: const Text('State department'),
              ),
            ],
          );
        })) {
      case Department.treasury:
        // Let's go.
        // ...
        break;
      case Department.state:
        // ...
        break;
    }
  }

  Future<void> _ListDialog() async {
    switch (await showDialog<Department>(
        context: context,
        builder: (BuildContext context) {
          var size = MediaQuery.of(context).size;

          return Center(
            child: Container(
                width: size.width * .8,
                height: size.height * .8,
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    color: Colors.blueAccent,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.all(Radius.circular(5))),
                //title: const Text('Select assignment'),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: size.width * 0.25,
                      child: ListView(
                        primary: false,
                        children: [
                          'a',
                          'b',
                          'c',
                          'd',
                          'b',
                          'c',
                          'd',
                          'b',
                          'c',
                          'd',
                          'b',
                          'c',
                          'd'
                        ].map<TagCell>((item) {
                          return TagCell(
                            icon: Icons.add,
                            title: item,
                            desc: item,
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                          );
                        }).toList(),
                      ),
                    ),
                    SizedBox(
                      width: 1,
                      child: VerticalDivider(
                        thickness: 0.5,
                        color: Colors.blue,
                      ),
                    ),
                    Expanded(
                      child: ListView(
                        primary: false,
                        children: [
                          'a',
                          'b',
                          'c',
                          'd',
                          'b',
                          'c',
                          'd',
                          'b',
                          'c',
                          'd',
                          'b',
                          'c',
                          'd'
                        ].map<TagCell>((item) {
                          return TagCell(
                            icon: Icons.add,
                            title: item,
                            desc: item,
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                          );
                        }).toList(),
                      ),
                    ),
                  ],
                )),
          );
        })) {
      case Department.treasury:
        // Let's go.
        // ...
        break;
      case Department.state:
        // ...
        break;
    }
  }

  Future<void> _WidgetDialog() async {
    switch (await showDialog<Department>(
        context: context,
        builder: (BuildContext context) {
          return PickerWidget();
        })) {
      case Department.treasury:
        // Let's go.
        // ...
        break;
      case Department.state:
        // ...
        break;
    }
  }

  Future<void> _neverSatisfied() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Rewind and remember'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('You will never be satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Regret'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

class PickerWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PickerWidgetState();
}

class _PickerWidgetState extends State<PickerWidget> {
  List<CategoryModel>? cates;

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration(seconds: 1), () {
      setState(() {
        cates = [
          CategoryModel(),
          CategoryModel(),
          CategoryModel(),
          CategoryModel(),
        ];
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return cates == null
        ? Center(child: CircularProgressIndicator())
        : Center(
            child: Container(
              width: size.width * .8,
              height: size.height * .8,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.all(Radius.circular(5))),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    width: size.width * 0.25,
                    child: ListView(
                      primary: false,
                      children: ['a', 'b', 'c', 'd'].map<TagCell>((item) {
                        return TagCell(
                          icon: Icons.add,
                          title: item,
                          desc: item,
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                        );
                      }).toList(),
                    ),
                  ),
                  SizedBox(
                    width: 1,
                    height: size.height,
                    child: VerticalDivider(
                      color: Colors.blueAccent,
                    ),
                  ),
                  Expanded(
                    child: ListView(
                      primary: false,
                      children: ['a', 'b', 'c', 'd'].map<TagCell>((item) {
                        return TagCell(
                          icon: Icons.add,
                          title: item,
                          desc: item,
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                        );
                      }).toList(),
                    ),
                  ),
                ],
              ),
            ),
          );
  }
}

class Department {
  static const Department treasury = Department();

  static const Department state = Department();

  const Department();
}
