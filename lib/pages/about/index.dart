import 'package:flutter/material.dart';
import '../../models/detail_arguments.dart';

class IndexPage extends StatefulWidget {
  final DetailArguments? arguments;

  const IndexPage({Key? key, this.arguments}) : super(key: key);
  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  String _title = "About";

  @override
  Widget build(BuildContext context) {
    //arguments = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text(_title),
      ),
      body: Column(children: [
        Center(
          child: Padding(
            padding: EdgeInsets.only(top: 30),
            child: Text('About '),
          ),
        ),
        Center(
          child: TextButton(
            onPressed: () {
              Navigator.pushNamed(context, '/swipe');
            },
            child: Text('Scene page'),
          ),
        ),
      ]),
    );
  }
}
