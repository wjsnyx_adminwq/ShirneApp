import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import '../../widgets/tags/empty.dart';
import '../../models/detail_arguments.dart';
import '../../models/product_model.dart';
import '../../utils/api.dart';

class DetailPage extends StatefulWidget {
  final DetailArguments arguments;

  const DetailPage({Key? key, required this.arguments}) : super(key: key);

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  String _title = "产品详情";
  int id = 0;
  ProductModel? product;
  bool isFavourite = false;
  String? title;
  String? image;

  @override
  void initState() {
    super.initState();
    var arguments = widget.arguments;
    if (arguments.temp != null) {
      if (arguments.temp is Map<String, dynamic>) {
        var art = arguments.temp as Map<String, dynamic>;
        this.title = art['title'];
        this.image = art['image'];
      } else if (arguments.temp is ProductModel) {
        product = arguments.temp as ProductModel;
        title = product!.title;
        image = product!.image;
      }
    }
    if (arguments.id > 0) {
      this.id = arguments.id;
      _loadData();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(_title),
      ),
      body: Column(
        children: <Widget>[
          this.image == null
              ? SizedBox(
                  height: 0,
                )
              : Center(
                  child: Hero(
                      tag: "product_" + this.id.toString(),
                      child: Image.network(image!, fit: BoxFit.cover)),
                ),
          Container(
            padding: EdgeInsets.all(10),
            child: product!.content.isEmpty
                ? TagEmpty('暂无介绍~')
                : Html(
                    data: product!.content,
                  ),
          ),
        ],
      ),
    );
  }

  _loadData() async {
    Api.post('product/view', {'id': this.id}).then((result) {
      var data = result.data;
      if (data == null) return;
      if (!mounted) return;
      setState(() {
        var pdata = result.data!['product'];
        pdata['skus'] = result.data!['skus'];
        pdata['images'] = result.data!['images'];
        product = ProductModel.fromJson(pdata);
        title = product!.title;
        image = product!.image;

        isFavourite = result.data!['is_favourite'];
      });
    });
  }
}
