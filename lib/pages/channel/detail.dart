
import 'package:flutter/material.dart';
import 'package:flutter_card_swipper/flutter_card_swiper.dart';
import 'package:flutter_html/flutter_html.dart';

import '../../utils/api.dart';
import '../../utils/tools.dart';
import '../../widgets/tags/empty.dart';
import '../../widgets/tags/icon.dart';
import '../../widgets/tags/title.dart';

import '../../models/article_model.dart';
import '../../application.dart';

class DetailPage extends StatefulWidget{
  final String? channel;
  final int? id;
  final Object? temp;

  DetailPage({Key? key,required Map<String, dynamic> arguments}) :
        channel = arguments['channel'],
        id = arguments['id'],
        temp = arguments['temp'],
        super(key: key);

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  bool isLoading = true;
  ArticleModel? article;

  String? title;
  String? cover;

  bool isFavourite = false;
  bool isDigg = false;

  late ScrollController _scrollController;
  bool titleMode = false;
  double swipeHeight = 300;

  @override
  void initState() {
    super.initState();

    if (widget.temp != null) {
      if (widget.temp is Map<String, dynamic>) {
        var art = widget.temp as Map<String, dynamic>;
        this.title = art['title'];
        this.cover = art['cover'];
      } else if (widget.temp is ArticleModel) {
        article = widget.temp as ArticleModel;
        title = article!.title;
        cover = article!.cover;
      }
    }

    _scrollController = ScrollController();
    _scrollController.addListener(_onScroll);

    if (widget.id != null) {
      _loadData();
    }
  }

  _onScroll() {
    if (_scrollController.offset > swipeHeight - 50) {
      if (!titleMode && article != null) {
        setState(() {
          titleMode = true;
        });
      }
    } else {
      if (titleMode) {
        setState(() {
          titleMode = false;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    swipeHeight = MediaQuery.of(context).size.width * 0.6;

    return Scaffold(
      extendBodyBehindAppBar: true,
      body: CustomScrollView(
        controller: _scrollController,
        slivers: <Widget>[
          SliverAppBar(
            pinned: true,
            backgroundColor: Colors.white,
            expandedHeight: swipeHeight,
            leading: IconButton(
              onPressed: () {
                Application.instance!.back(context);
              },
              icon: TagIcon(
                Icons.arrow_back,
                size: 24,
                color: titleMode ? Colors.black87 : Colors.white,
                shadows: titleMode
                    ? null
                    : [
                  Shadow(
                      color: Color.fromRGBO(0, 0, 0, 0.8),
                      offset: Offset.zero,
                      blurRadius: 10)
                ],
              ),
            ),
            title: Text(
              titleMode ? title! : '',
              maxLines: 1,
              style: TextStyle(color: Colors.black87),
              overflow: TextOverflow.ellipsis,
            ),
            flexibleSpace: FlexibleSpaceBar(
              background: (article!.images == null || article!.images!.isEmpty)
                  ? Tools.image(article!.cover)
                  : _buildSwipe(context),
              //titlePadding: EdgeInsets.only(right: 50.0),
              //centerTitle: true,
              collapseMode: CollapseMode.parallax,
            ),
          ),
          SliverList(
              delegate: SliverChildListDelegate([
                Center(
                  child: TagTitle(
                    title!,
                    fontSize: 24,
                  ),
                ),
                Center(
                  child: RichText(
                    text: TextSpan(children: [
                      TextSpan(text: '类目:'),
                      TextSpan(text: article!.cateId.toString()),
                      TextSpan(text: ' 发布日期:'),
                      TextSpan(
                          text: Tools.formatDateFromTimestamp(article!.createTime)),
                      TextSpan(text: ' 浏览:'),
                      TextSpan(text: article!.views.toString()),
                    ], style: TextStyle(color: Colors.black38, fontSize: 13)),
                    softWrap: false,
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: article!.content.isEmpty
                      ? (isLoading
                      ? Center(child: CircularProgressIndicator())
                      : TagEmpty(
                    '暂无内容~',
                    icon: null,
                  ))
                      : Html(
                    data: article!.content,
                  ),
                )
              ])),
        ],
      ),
    );
  }

  _buildSwipe(BuildContext context) {
    return Swiper(
      itemBuilder: (BuildContext context, idx) {
        return Tools.image(article!.images![idx].image);
      },
      itemCount: article!.images!.length,
      control: SwiperControl(),
    );
  }

  _loadData() async {
    Api.post('article/view', {'id': widget.id!}).then((result) {
      var data = result.data;
      if (data == null) return;
      if (!mounted) return;
      setState(() {
        var pdata = result.data!['article'];
        pdata['images'] = result.data!['images'];
        article = ArticleModel.fromJson(pdata);
        title = article!.title;
        cover = article!.cover;

        isFavourite = result.data!['is_favourite'] == 1;
        isDigg = result.data!['digged'] == 1;
        isLoading = false;
      });
    });
  }

}