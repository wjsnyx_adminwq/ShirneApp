
import 'package:flutter/material.dart';
import 'package:shirne_app/models/category_model.dart';

import '../../application.dart';

class IndexPage extends StatefulWidget{
  final String channel;

  const IndexPage({Key? key,required this.channel}) : super(key: key);

  @override
  State<IndexPage> createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  late CategoryModel channel;
  late List<CategoryModel> categories;
  bool initialized = false;

  @override
  void initState() {
    super.initState();
    initialize();
  }

  @override
  Widget build(BuildContext context) {
    if(!initialized){
      return Center(child: CircularProgressIndicator(),);
    }
    return Scaffold(
      appBar: AppBar(title: Text(channel.title),),
      body: Center(
        child: Text(channel.title),
      ),
    );
  }

  void initialize() async {
    Application.instance!.getCategories().then((value) {
      if(value[0] != null){
        channel = value[0]!.firstWhere((element) => element.name == widget.channel, orElse:() => CategoryModel());
        categories = value[channel.id]!;
        initialized = true;
        if(mounted){
          setState(() {});
        }
      }
    });
  }

}