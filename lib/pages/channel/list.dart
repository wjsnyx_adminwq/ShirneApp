
import 'package:flutter/material.dart';
import 'package:shirne_app/models/article_model.dart';
import 'package:shirne_app/models/category_model.dart';
import 'package:shirne_app/utils/api.dart';
import 'package:shirne_app/utils/tools.dart';
import 'package:shirne_app/widgets/tags/divider.dart';
import 'package:shirne_app/widgets/tags/empty.dart';

import '../../application.dart';

class ListPage extends StatefulWidget{
  final String? channel;
  final int? cateId;

  ListPage({Key? key,required Map<String, dynamic> arguments}) :
        channel = arguments['channel'],
        cateId = arguments['cate'],
        super(key: key);

  @override
  State<ListPage> createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  late CategoryModel channel;
  late List<CategoryModel> categories;
  bool initialized = false;

  int cateId = 0;

  int page = 1;
  int pageSize = 10;
  int total = 0;

  List<ArticleModel>? _list;
  bool isLoading = true;
  bool hasMore = true;

  late ScrollController _scrollController;
  TabController? _tabController;

  @override
  void initState() {
    super.initState();
    initialize().then((){
      if(!mounted)return;
      if (widget.cateId != null) {
        this.cateId = widget.cateId!;
      }

      _scrollController = ScrollController();
      _scrollController.addListener(_onScroll);

      _list = [];
    });
  }

  _onScroll() {
    if (!isLoading) {
      //print(_scrollController.position);
      if (hasMore &&
          _scrollController.position.pixels + 10 >=
              _scrollController.position.maxScrollExtent) {
        setState(() {
          isLoading = true;
        });
        page++;
        _loadData();
      }
    }
  }

  _changeCate(int cateId) {
    setState(() {
      this.cateId = cateId;
    });
    _reloadData();
  }

  @override
  Widget build(BuildContext context) {
    if(!initialized){
      return Center(child: CircularProgressIndicator(),);
    }
    return Scaffold(
      appBar: AppBar(title: Text(channel.title),),
      body: Center(
        child: Text(channel.title),
      ),
    );
  }

  initialize() async {
    var cates = await Application.instance!.getCategories();
    if(cates[0] != null){
      channel = cates[0]!.firstWhere((element) => element.name == widget.channel, orElse:() => CategoryModel());
      categories = cates[channel.id]!;
      initialized = true;
      if(mounted){
        setState(() {});
      }
    }
  }

  _renderList() {
    if (_list == null || _list!.isEmpty) {
      if (isLoading) {
        return Center(
          child: CircularProgressIndicator(),
        );
      }
      return TagEmpty('该类目暂无文章');
    }

    List<Widget> childs = _list!.map<Widget>((art) {
      return Card(
        child: ListTile(
          onTap: () {
            Application.instance!.route('/article/detail',
                arguments: {'id': art.id, 'data': art});
          },
          leading: (art.cover != null && art.cover.isNotEmpty)
              ? Tools.image(art.cover, width: 80, height: 60)
              : null,
          title: Text(art.title),
          subtitle: Text(
            art.description,
            maxLines: 3,
          ),
        ),
      );
    }).toList();

    if (isLoading) {
      childs.add(Padding(
        padding: EdgeInsets.symmetric(vertical: 10),
        child: Center(
          child: SizedBox(
            height: 20,
            child: CircularProgressIndicator(
              strokeWidth: 2,
            ),
          ),
        ),
      ));
    } else if (!hasMore) {
      childs.add(TagDivider('没有更多了~'));
    }

    return Expanded(
      child: ListView(
        controller: _scrollController,
        children: childs,
      ),
    );
  }

  _reloadData() {
    setState(() {
      page = 1;
      hasMore = true;
      isLoading = true;
      _list!.clear();
    });
    _loadData();
  }

  _loadData() async {
    Api.post('article/get_list', {
      'cate': this.cateId,
      'page': this.page,
      'pagesize': this.pageSize
    }).then((result) {
      if (!mounted) return;
      var lists = result.data!['lists'];
      //print(lists);
      setState(() {
        isLoading = false;
        total = Tools.parseInt(result.data!['total']);
        if (lists != null) {
          _list = _list! +
              lists
                  .map<ArticleModel>((item) => ArticleModel.fromJson(item))
                  .toList();
        }
        if (lists == null ||
            lists.length < pageSize ||
            _list!.length >= total) {
          hasMore = false;
        }
      });
    }).catchError((err) {
      setState(() {
        isLoading = false;
      });
    });
  }

}