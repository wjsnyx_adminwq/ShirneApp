import 'package:flutter/material.dart';
import 'package:shirne_app/application.dart';
import 'home/home.dart' show HomePage;
import 'product/index.dart' as product;
import 'article/index.dart' as article;
import 'about/index.dart' as about;
import 'test/index.dart' as test;

class IndexPage extends StatefulWidget {
  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  int _selectedIndex = 0;
  int lastTapBack = 0;
  List<Widget> _widgetOptions = <Widget>[
    HomePage(),
    product.IndexPage(),
    article.IndexPage(),
    about.IndexPage(),
    test.IndexPage(),
  ];

  void _onItemTapped(index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    Application.instance!.setContext(context);

    return WillPopScope(
      child: Scaffold(
        body: _widgetOptions[_selectedIndex],
        bottomNavigationBar: BottomNavigationBar(
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.home), label: '首页'),
            BottomNavigationBarItem(icon: Icon(Icons.widgets), label: '产品'),
            BottomNavigationBarItem(icon: Icon(Icons.event_note), label: '文章'),
            BottomNavigationBarItem(icon: Icon(Icons.work), label: '关于'),
            BottomNavigationBarItem(icon: Icon(Icons.extension), label: '实验室')
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.blue,
          onTap: _onItemTapped,
          type: BottomNavigationBarType.fixed,
          unselectedItemColor: Colors.grey,
          selectedFontSize: 12,
          unselectedFontSize: 12,
          showUnselectedLabels: true,
        ),
      ),
      onWillPop: () {
        int now = DateTime.now().millisecondsSinceEpoch;
        print('onWillPop ' + lastTapBack.toString() + ' ' + now.toString());
        if (now - lastTapBack > 800) {
          lastTapBack = now;
          Application.instance!.toast('再次点击返回退出应用', 1);
          return Future.value(false);
        } else {
          return Future.value(true);
        }
      },
    );
  }
}
