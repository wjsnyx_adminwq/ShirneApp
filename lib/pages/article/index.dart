import 'package:flutter/material.dart';
import '../../application.dart';
import '../../utils/api.dart';
import '../../widgets/card_widget.dart';
import '../../widgets/tags/empty.dart';
import '../../models/category_model.dart';

class IndexPage extends StatefulWidget {
  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  String _title = "文章";

  bool isLoading = true;
  Map<int, List<CategoryModel>>? _cates;

  @override
  void initState() {
    super.initState();
    print('init state');
    _loadCategory();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(_title),
      ),
      body: (_cates == null || _cates!.isEmpty || _cates![0]!.isEmpty)
          ? (isLoading
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : TagEmpty('暂无分类'))
          : ListView(
              children: _cates![0]!.map<Widget>((cateItem) {
                return CardWidget(
                  title: Row(
                    children: <Widget>[
                      Text(cateItem.title),
                      Spacer(),
                      MaterialButton(
                          padding: EdgeInsets.all(0),
                          onPressed: () {
                            Application.instance!.route('/article/list?category=' +
                                cateItem.id.toString());
                          },
                          child: Row(
                            children: <Widget>[
                              Text('查看'),
                              Icon(
                                Icons.arrow_forward_ios,
                                size: 16,
                              )
                            ],
                          ))
                    ],
                  ),
                  child: (_cates!.containsKey(cateItem.id) &&
                          _cates![cateItem.id]!.isNotEmpty)
                      ? Wrap(
                          children: _cates![cateItem.id]!.map<Widget>((item) {
                            return TextButton(
                              onPressed: () {
                                Application.instance!.route('/article/list', arguments: {
                                  'top_category': cateItem.id,
                                  'category': item.id
                                });
                              },
                              child: Text(item.title),
                            );
                          }).toList(),
                        )
                      : TagEmpty(
                          '没有二级类目',
                          icon: null,
                        ),
                );
              }).toList(),
            ),
    );
  }

  _loadCategory() async {
    Api.post('article/get_all_cates').then((result) {
      if (!mounted) return;
      if (result.data != null) {
        _cates = CategoryModel.mapTree(result.data);
      }
      if (result.list != null) {
        _cates = CategoryModel.mapTree(result.list);
      }

      isLoading = false;

      if (!mounted) return;
      setState(() {});
    }).catchError((err) {
      print(err);
      if (!mounted) return;
      setState(() {
        isLoading = false;
      });
    });
  }
}
