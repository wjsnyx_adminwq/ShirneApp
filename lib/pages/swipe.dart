import 'package:flutter/material.dart';
import 'package:flutter_card_swipper/flutter_card_swiper.dart';
import '../config.dart';

class SwipePage extends StatefulWidget {
  @override
  _SwipePageState createState() => _SwipePageState();
}

class _SwipePageState extends State<SwipePage> {
  List<String> _guideList = [
    'https://shirne.oss-cn-shenzhen.aliyuncs.com/website-mapp/wall/wall-5.jpg',
    'https://shirne.oss-cn-shenzhen.aliyuncs.com/website-mapp/wall/wall-3.jpg',
    'https://shirne.oss-cn-shenzhen.aliyuncs.com/website-mapp/wall/wall-7.jpg',
    'https://shirne.oss-cn-shenzhen.aliyuncs.com/website-mapp/wall/wall-2.jpg',
  ];
  List<String> _guideInfoList = [
    "岁月悠长",
    "山河无恙",
    "君如皎月",
    "我如微尘",
  ];

  double _btnPadding = Config.windowHeight * .5;
  double _guidePadding = Config.windowHeight * .3;

  _gotoHome(BuildContext context) {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Swiper(
          itemBuilder: (BuildContext context, int index) {
            return _SceneWidget(_guideList[index], _guideInfoList[index]);
          },
          itemCount: _guideList.length,
          pagination: new SwiperPagination(),
          loop: false,
          onIndexChanged: (index) {
            if (index == _guideList.length - 1) {
              setState(() {
                _btnPadding = Config.windowHeight * .22;
                _guidePadding = Config.windowHeight * .5;
              });
            } else {
              setState(() {
                _btnPadding = Config.windowHeight * .5;
                _guidePadding = Config.windowHeight * .3;
              });
            }
          },
          //control: new SwiperControl(),
        ),
        Center(
          child: AnimatedPadding(
            padding: EdgeInsets.only(top: _guidePadding),
            duration: Duration(milliseconds: 500),
            curve: Curves.easeInOut,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  '向右滑动',
                  style: TextStyle(
                      fontSize: 12,
                      color: Color.fromRGBO(255, 255, 255, 0.4),
                      decoration: TextDecoration.none,
                    ),
                ),
                Icon(
                  Icons.arrow_right,
                  color: Color.fromRGBO(255, 255, 255, 0.4),
                )
              ],
            ),
          ),
        ),
        Center(
          child: AnimatedPadding(
            padding: EdgeInsets.only(top: _btnPadding),
            duration: Duration(milliseconds: 500),
            curve: Curves.easeInOut,
            child: ElevatedButton(
              onPressed: () {
                _gotoHome(context);
              },
              child: Text('Go !'),
            ),
          ),
        ),
      ],
    );
  }
}

class _SceneWidget extends StatelessWidget {
  _SceneWidget(this._image, this._title) : super();

  final String _image;
  final String _title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      body: Stack(
        children: <Widget>[
          Image.network(
            _image,
            fit: BoxFit.cover,
            width: double.infinity,
            height: double.infinity,
          ),
          Center(
            child: Text(
              _title,
              style: TextStyle(
                  fontSize: 20,
                  color: Color.fromRGBO(255, 255, 255, 0.6),
                  decoration: TextDecoration.none,
                ),
            ),
          )
        ],
      ),
    );
  }
}
