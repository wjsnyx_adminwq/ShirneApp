import '../utils/tools.dart';

import 'sku_model.dart';
import 'image_model.dart';

class ProductModel {
  int id = 0;
  String? lang;
  int mainId = 0;
  int cateId = 0;
  int brandId = 0;

  String title = '';
  String viceTitle = '';
  String goodsNo = '';
  String image = '';

  List<ImageModel>? images;
  List<SkuModel>? skus;
  Map<String, dynamic>? specData;
  Map<String, dynamic>? propData;

  double maxPrice = 0;
  double minPrice = 0;
  double marketPrice = 0;

  String content = '';

  int createTime = 0;
  int updateTime = 0;
  int levelId = 0;
  List<int>? levels;

  int storage = 0;
  int postageId = 0;
  double postage = 0;

  int sale = 0;
  int type = 0;

  int isCommission = 0;
  int isDiscount = 0;
  int status = 0;

  dynamic commissionPercent;

  ProductModel();

  ProductModel.fromJson(Map? product) {
    if (product == null || !product.containsKey('id')) return;

    this.id = product['id'];
    this.cateId = product['cate_id'];
    this.brandId = product['brand_id'];
    this.title = product['title'];
    this.viceTitle = product['vice_title'];
    this.image = Tools.fixImageUrl(product['image']);

    this.maxPrice = product['max_price'];
    this.minPrice = product['min_price'];

    this.content = Tools.fixContentImageUrl(product['content']);

    if (product.containsKey('prop_data') && product['prop_data'] is Map) {
      this.propData = product['prop_data'];
    }
    if (product.containsKey('spec_data') && product['spec_data'] is Map) {
      this.specData = product['spec_data'];
    }

    List<dynamic>? images = product['images'];
    if (images != null && images.isNotEmpty) {
      this.images = images.map((img) => ImageModel.fromJson(img)).toList();
    }

    List<dynamic>? skus = product['skus'];
    if (skus != null && skus.isNotEmpty) {
      this.skus = skus.map((img) => SkuModel.fromJson(img)).toList();
    }
  }
}
