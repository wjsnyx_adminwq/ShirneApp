class ListResult {
  List<dynamic>? lists;
  int page = 1;
  int count = 0;
  int total = 0;
  int totalPage = 0;

  ListResult.fromJson(Map<String, dynamic>? json) {
    if (json == null || !json.containsKey('lists')) {
      return;
    }

    this.lists = json['lists'] as List<dynamic>;
    if (json.containsKey('page')) {
      this.page = json['page'];
    }
    if (json.containsKey('count')) {
      this.count = json['count'];
    }
    if (json.containsKey('total')) {
      this.total = json['total'];
    }
    if (json.containsKey('total_page')) {
      this.totalPage = json['total_page'];
    }
  }

  bool hasList() {
    return lists != null && lists!.length > 0;
  }
}
