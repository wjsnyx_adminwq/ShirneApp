import '../utils/tools.dart';

class SearchArguments {
  String? keyword = '';
  int category = 0;
  int topCategory = 0;

  SearchArguments({this.keyword, this.category = 0});

  SearchArguments.fromObject(Map<String, dynamic>? object) {
    if (object == null) return;
    this.keyword = object['keyword'];
    this.category = Tools.parseInt(object['category']);
    this.topCategory = Tools.parseInt(object['top_category']);
    if (category > 0 && topCategory <= 0) {
      topCategory = category;
    }
  }
}
