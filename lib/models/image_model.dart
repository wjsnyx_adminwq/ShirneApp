import '../utils/tools.dart';

class ImageModel {
  int id = 0;
  String title = '';
  String image = '';
  String description = '';
  int sort = 0;

  int articleId = 0;
  int productId = 0;

  ImageModel.fromJson(Map image) {
    this.id = image['id'];
    this.title = image['title'];
    this.description = image['description'];
    this.image = Tools.fixImageUrl(image['image']);
    this.sort = image['sort'];

    this.articleId = image['article_id'];
    this.productId = image['product_id'];
  }
}
