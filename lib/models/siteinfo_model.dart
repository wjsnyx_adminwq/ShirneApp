

import '../utils/tools.dart';

class SiteinfoModel{
  String webname = '';
  String keywords = '';
  String description = '';
  String weblogo = '';
  String url = '';
  String name = '';
  String address = '';
  String location = '';
  String shareimg = '';

  SiteinfoModel();
  
  SiteinfoModel.fromJson(Map siteinfo){
    this.webname = siteinfo['webname'];
    this.keywords = siteinfo['keywords'];
    this.description = siteinfo['description'];
    this.weblogo = Tools.fixImageUrl(siteinfo['weblogo']);
    this.shareimg = Tools.fixImageUrl(siteinfo['shareimg']);
    
    this.url = siteinfo['url'];
    this.name = siteinfo['name'];
    this.address = siteinfo['address'];
    this.location = siteinfo['location'];
  }
}