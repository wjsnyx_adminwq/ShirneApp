

import '../utils/tools.dart';

class CategoryModel{
  int id = 0;
  int pid = 0;
  String title = '';
  String short = '';
  String name = '';
  String icon = '';
  String image = '';
  String keywords = '';
  String description = '';
  int pagesize = 0;
  int sort = 0;

  CategoryModel();

  CategoryModel.fromJson(Map category){
    this.id = Tools.parseInt(category['id']);
    this.pid = Tools.parseInt(category['pid']);
    this.title = category['title'];
    this.short = category['short'];
    this.name = category['name'];
    this.keywords = category['keywords'];
    this.description = category['description'];
    this.icon = Tools.fixImageUrl(category['icon']);
    this.image = Tools.fixImageUrl(category['image']);
    this.pagesize = Tools.parseInt(category['pagesize']);
    this.sort = Tools.parseInt(category['sort']);
  }

  static Map<int, List<CategoryModel>> mapTree(dynamic lists){
    if(lists == null)return Map<int, List<CategoryModel>>();

    if(lists is List<dynamic>){

      return lists.asMap().map<int, List<CategoryModel>>((key, item) => MapEntry(
              Tools.parseInt(key),
              item
                  .map<CategoryModel>((cate) => CategoryModel.fromJson(cate))
                  .toList()));

    }else if(lists is Map<int, dynamic>){
      return lists.map<int, List<CategoryModel>>((key, item) => MapEntry(
              Tools.parseInt(key),
              item
                  .map<CategoryModel>((cate) => CategoryModel.fromJson(cate))
                  .toList()));
    }else{
      return Map<int, List<CategoryModel>>();
    }
  }
}