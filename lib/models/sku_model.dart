import '../utils/tools.dart';

class SkuModel {
  int skuId = 0;
  String goodsNo = '';
  String image = '';
  double price = 0;
  double marketPrice = 0;
  double costPrice = 0;
  Map? extPrice;
  int storage = 0;
  int weight = 0;
  String size = '';

  int productId = 0;

  SkuModel.fromJson(Map sku) {
    this.skuId = sku['sku_id'];
    this.goodsNo = sku['goods_no'];
    this.price = sku['price'];
    this.marketPrice = sku['market_price'];
    this.costPrice = sku['cost_price'];
    if (sku.containsKey('ext_price') && sku['ext_price'] is Map) {
      this.extPrice = sku['ext_price'];
    }
    this.storage = sku['storage'];
    this.weight = sku['weight'];
    this.size = sku['size'];

    this.image = Tools.fixImageUrl(sku['image']);

    this.productId = sku['product_id'];
  }
}
