import '../utils/tools.dart';

class DetailArguments {
  int id = 0;
  Object? temp;

  DetailArguments({required this.id, this.temp});

  DetailArguments.fromObject(Map<String, dynamic>? object) {
    if (object == null) return;
    this.id = Tools.parseInt(object['id']);
    if (object.containsKey('data')) {
      this.temp = object['data'];
    }
  }
}
