import '../utils/tools.dart';

class MemberModel {
  int id = 0;
  String username = '';
  String nickname = '';
  String realname = '';
  int levelId = 0;
  String mobile = '';
  String email = '';
  String avatar = '';
  int gender = 0;
  int birth = 0;

  String province = '';
  String city = '';
  String county = '';
  String address = '';

  int isAgent = 0;
  int referer = 0;
  String agentCode = '';

  int credit = 0;
  int money = 0;
  int reward = 0;
  int frozeCredit = 0;
  int frozeMoney = 0;
  int frozeReward = 0;

  MemberModel();

  MemberModel.fromJson(Map userinfo) {
    this.id = userinfo['id'];
    this.username = userinfo['username'];
    this.nickname = userinfo['nickname'];
    this.avatar = Tools.fixImageUrl(userinfo['avatar']);

    if (userinfo.containsKey('realname')) {
      this.realname = userinfo['realname'];
    }

    this.levelId = userinfo['level_id'];

    if (userinfo.containsKey('mobile')) {
      this.mobile = userinfo['mobile'];
      this.email = userinfo['email'];
      this.gender = userinfo['gender'];
      this.birth = userinfo['birth'];
    }

    if (userinfo.containsKey('province')) {
      this.province = userinfo['province'];
      this.city = userinfo['city'];
      this.county = userinfo['county'];
      this.address = userinfo['address'];
    }

    this.isAgent = userinfo['is_agent'];
    this.referer = userinfo['referer'];

    if (userinfo.containsKey('agentcode')) {
      this.agentCode = userinfo['agentcode'];
    }

    if (userinfo.containsKey('money')) {
      this.credit = userinfo['credit'];
      this.money = userinfo['money'];
      this.reward = userinfo['reward'];
      this.frozeCredit = userinfo['froze_credit'];
      this.frozeMoney = userinfo['froze_money'];
      this.frozeReward = userinfo['froze_reward'];
    }
  }
}
