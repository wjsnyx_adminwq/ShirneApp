import '../utils/tools.dart';

import 'image_model.dart';

class ArticleModel {
  int id = 0;
  String lang = '';
  int mainId = 0;
  int cateId = 0;

  String title = '';
  String viceTitle = '';
  String cover = '';
  String description = '';

  List<ImageModel>? images;
  Map<String, dynamic>? propData;

  String content = '';

  int createTime = 0;
  int updateTime = 0;

  int status = 0;
  int type = 0;
  int views = 0;

  ArticleModel.fromJson(Map? article) {
    if (article == null || !article.containsKey('id')) return;

    this.id = Tools.parseInt(article['id']);
    this.cateId = article['cate_id'] ?? 0;
    this.title = article['title'] ?? '';
    this.viceTitle = article['vice_title'] ?? '';
    this.cover = Tools.fixImageUrl(article['cover'] ?? '');

    this.description = article['description'] ?? '';
    this.content = Tools.fixContentImageUrl(article['content'] ?? '');

    this.createTime = Tools.parseInt(article['create_time']);
    this.updateTime = Tools.parseInt(article['update_time']);

    this.status = Tools.parseInt(article['status']);
    this.type = Tools.parseInt(article['type']);
    this.views = Tools.parseInt(article['views']);

    if (article.containsKey('prop_data') && article['prop_data'] is Map) {
      this.propData = article['prop_data'];
    }

    List<dynamic>? images = article['images'];
    if (images != null && images.isNotEmpty) {
      this.images =
          images.map<ImageModel>((img) => ImageModel.fromJson(img)).toList();
    }
  }
}
