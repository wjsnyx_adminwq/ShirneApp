// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:shirne_app/main.dart';

void main() {
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(ShirneApp());

    // Verify that our counter starts at 0.
    expect(find.text('0'), findsOneWidget);
    expect(find.text('1'), findsNothing);

    // Tap the '+' icon and trigger a frame.
    await tester.tap(find.byIcon(Icons.add));
    await tester.pump();

    // Verify that our counter has incremented.
    expect(find.text('0'), findsNothing);
    expect(find.text('1'), findsOneWidget);
  });

  test('test time', () {
    print(DateTime.now().millisecondsSinceEpoch.toString());
    print(DateTime.now().microsecondsSinceEpoch.toString());
  });

  test('test complete', () async {
    var complete = Completer();
    complete.future.then((val) {
      print('future success:' + val);
    });
    print('before delay');
    var future = Future.delayed(Duration(seconds: 2), () {
      complete.complete('aaa');
      print('after complete');
      complete.future.then((val) {
        print('future3 success:' + val);
      });
      complete = Completer();
      complete.future.then((val) {
        print('future reconstruct success:' + val);
      });
      complete.complete('bbb');
    });

    print('after delay');
    complete.future.then((val) {
      print('future2 success:' + val);
    });

    await future;
  });

  test('test future', () async {
    Function callback = () {
      print('这里是回调');
    };

    var future = Future.delayed(Duration(seconds: 2), () {
      print('future');
    });

    future.then((val) {
      callback();
    });

    await future;
  });

  test('test regexp', () {
    String html = r'''<p>
    苏宁易购818狂欢购物节
</p>
<p>
    <br/>
</p>
<p>
    <span style="color: rgb(25, 25, 25); font-family: &quot;PingFang SC&quot;, Arial, 微软雅黑, 宋体, simsun, sans-serif; background-color: rgb(255, 255, 255);">海外网8月26日电 于8月24日召开的七国集团（G7）峰会将于26日结束，日前，与会各国领导人按照惯例拍摄了“全家福”，不过英国媒体却似乎不太开心。他们注意到，英首相鲍里斯·约翰逊在合影中站在靠边的位置，而在领导人的伴侣们加入拍照后，约翰逊更是被挤到了更加边缘的地方。</span>
</p>
<p style="text-align: center;">
    <span style="color: rgb(25, 25, 25); font-family: &quot;PingFang SC&quot;, Arial, 微软雅黑, 宋体, simsun, sans-serif; background-color: rgb(255, 255, 255);"><img src="/uploads/ueditor/image/20190826/1566812191632749.png" title="1566812191632749.png" alt="s_img6.png"/></span>
</p>
<p>
    <br/>
</p>
<p style="border: 0px; margin-top: 0.63em; margin-bottom: 1.8em; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; color: rgb(25, 25, 25); font-family: &quot;PingFang SC&quot;, Arial, 微软雅黑, 宋体, simsun, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);">
    约翰逊与身旁的南非总统聊天（图源：GETTY）
</p>
<p style="border: 0px; margin-top: 0.63em; margin-bottom: 1.8em; padding: 0px; counter-reset: list-1 0 list-2 0 list-3 0 list-4 0 list-5 0 list-6 0 list-7 0 list-8 0 list-9 0; color: rgb(25, 25, 25); font-family: &quot;PingFang SC&quot;, Arial, 微软雅黑, 宋体, simsun, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);">
    据英国《每日邮报》报道，当地时间25日晚，G7峰会与会国领导人在法国城市比亚里茨合影。约翰逊站在南非总统拉马弗萨身旁，成为了正式拍照中最左侧的一位嘉宾。法国总统马克龙、美国总统特朗普、德国总理默克尔和加拿大总理特鲁多则是站在中间的位置。
</p>
<p>
    <span style="color: rgb(25, 25, 25); font-family: &quot;PingFang SC&quot;, Arial, 微软雅黑, 宋体, simsun, sans-serif; background-color: rgb(255, 255, 255);"><br/></span>
</p>
<p style="text-align: center;">
    <span style="color: rgb(25, 25, 25); font-family: &quot;PingFang SC&quot;, Arial, 微软雅黑, 宋体, simsun, sans-serif; background-color: rgb(255, 255, 255);"><img src="/uploads/ueditor/image/20190826/1566812225660750.png" title="1566812225660750.png" alt="s_img12.png"/></span><br/>
</p>''';

    html = html.replaceAllMapped(RegExp(r'src="([^"]+)"'), (Match match) {
      print(match);
      return 'src="' + 'prefix' + (match[1]!) + '"';
    });

    print(html);
  });
}
