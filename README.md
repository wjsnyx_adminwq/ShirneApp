# shirnecms web app

基于Flutter开发的企业官网APP, 后端项目 [ShirneCMS](https://gitee.com/shirnecn/ShirneCMS).

## Flutter

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


## 功能

首页展示轮播图, 分类, 推荐文章/产品

产品/文章  分类页 (模块首页)

列表页 (分类切换，无限加载)

详情页 (Sliver滚动效果)

### 实验室 

一些布局测试,官方文档的效果接入  弹框效果 GPS定位  搜索弹框效果


## 演示截图
